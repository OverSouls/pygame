import os
import xml.etree.ElementTree as ET

def renamer(a, b):
    path = "/Users/timofei/programs/py/Jean/resources/images/flags-normal"

    end = ".png"
    old_file = os.path.join(path, a + end)
    new_file = os.path.join(path, b + end)

    os.rename(old_file, new_file)


if __name__ == '__main__':
    tree = ET.parse('/Users/timofei/programs/py/Jean/resources/names.xml').getroot()

    countrys = []
    countrys_dict = {}

    for country in tree.getchildren():
        a = ""
        b = ""
        for elem in country.getchildren():
            if elem.tag == 'english':
                if not elem.text:
                    b = "None"
                else:
                    b = elem.text.lower()
                print(elem.tag + " => " + b)
            if elem.tag == 'alpha2':
                if not elem.text:
                    a = "None"
                else:
                    a = elem.text.lower()
                print(elem.tag + " => " + a)
        if country.tag == "country":
            countrys_dict[a] = b
            try:
                renamer(a, b)
                countrys.append(countrys_dict)
            except FileNotFoundError:
                print(a, " ", b)
            countrys_dict = {}
