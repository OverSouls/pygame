from src.func.loadImage import load_image
from src.func.colors import white  # импорт цвета
import pygame as pg


class Button:
    x1, y1 = 0, 0  # правый угол
    x2, y2 = 0, 0  # левый угол
    xt, yt = 0, 0  # координаты текста
    text = None  # текст
    image = None  # катинка кнопки
    state = 0  # состояние 0 не нажата/ 1 мышь наведена/ 2,3  нажата красный,зеленый
    flag = False  # правельный ли ответ за кнопкой если true то да

    def __init__(self, text, x, y, xt, yt):
        # запись координат
        self.x1 = x
        self.y1 = y
        self.x2 = x + 235
        self.y2 = y + 40
        self.xt = x + xt
        self.yt = y + yt
        # шрифт для текста
        font = pg.font.Font(None, 30, bold=False, italic=True)
        # если текст слишком длинный укоротить
        if len(text) > 21:
            text = text[:20] + ".."
        # создание текста
        self.text = font.render(text, True, [225, 225, 205])
        # картинка кнопки по умолчанию
        self.image = pg.transform.scale(load_image("buttons", "button.png")[0], (245, 40))
        # обрезка белого фона
        self.image.set_colorkey(white)

    def draw(self, screen):
        # рисуем текст и картинку в координатах
        screen.blit(self.image, [self.x1, self.y1])
        screen.blit(self.text, [self.xt, self.yt])

    def change(self, state):
        if self.state != state and self.state != 2:  # если не нажат то изменить картинку
            self.state = state
            if state == 0:  # если мышь не наведена
                self.image, b = load_image("buttons", "button.png")  # новая картинка
            else:  # если наведена
                self.image, b = load_image("buttons", "button-3.png")  # тоже новая картика но тип мышь наведена эфект
            self.image.set_colorkey(white)  # обрезка фона

    def pressed(self):  # нажатие на кнопку
        self.state = 2  # состояние 2 = нажата картинка менятся не будет больше
        if self.flag:  # если правильный ответ то зеленым
            self.image, b = load_image("buttons", "button-4.png")  # новая картинка
        else:  # иначе красным
            self.image, b = load_image("buttons", "button-2.png")  # новая картинка
        self.image.set_colorkey(white)  # обрезка фона
        return self.flag

    def is_it(self, x, y):  # проверка принадлежит x и y кнопке
        if self.x1 <= x and self.y1 <= y and self.x2 >= x and self.y2 >= y:
            self.change(1)  # показать что на нее навелися
            return True
        else:
            self.change(0)  # обычное состояние
            return False
