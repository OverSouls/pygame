import pygame as pg
from src.buttons.button import Button  # подключение кнопок
from src.func.loadImage import load_image  # подключение функции загрузки изображений
from src.screens.screen import Screen  # подключ. основного экрана
from src.func.get_name import get_name  # подключение функции случаайной странны
import time  # время


class MainScreen(Screen):
    button1 = None  # первая кнопка
    button2 = None  # вторая кнопка
    start_time = 0  # время последнея смены картинки
    image = None  # картинка

    def __init__(self):
        super().__init__("MainScreen", 0, "map.jpg", self.draw)  # передаем основному screen имя , state по умолчанию -
        # имя картинки и функцию рисования деталий

        self.button1 = Button("Start", 192.5, 180, 100, 12)  # создание кнопок с надписяю в координатах
        self.button2 = Button("Score", 192.5, 280, 100, 12)  #

        font = pg.font.Font(None, 100, bold=False, italic=True)  # вызов шрифта для написания текста
        #                            \/текст     \/сглаживание  \/цвет (r,g,b)
        self.text = font.render("Check yourself", True, [30, 60, 80])  # создание текста
        self.start_time = time.time()  # запись времени
        #                               \/загрузка изображения флага по случайному имени   \/ изменение размера картинки
        self.image = pg.transform.scale(load_image('flags-normal', get_name() + ".png")[0], (550, 350))

    def draw(self, screen):
        if time.time() - self.start_time > 4:  # прошло ли четыре секунды с последней смены
            self.start_time = time.time()  # новое время
            #                           \/загрузка изображения флага по случайному имени   \/ изменение размера картинки
            self.image = pg.transform.scale(load_image('flags-normal', get_name() + ".png")[0], (550, 350))

        screen.blit(self.image, (45, 350))  # рисуем изображение

        screen.blit(self.text, (80, 60))  # рисуем текст

        self.button1.is_it(self.mouse_x, self.mouse_y)  # проверка находится ли мышь над кнопкой
        self.button2.is_it(self.mouse_x, self.mouse_y)

        self.button1.draw(screen)  # рисование кнопки
        self.button2.draw(screen)

    def events_processing(self, events):
        for event in events:
            if self.off_check(event):  # проверка не нажат ли крестик
                return -1
            elif event.type == pg.MOUSEBUTTONDOWN:  # проверка был ли клик мышки если был то
                if self.button1.is_it(self.mouse_x, self.mouse_y):  # по кнопке ли был клик
                    return 1  # переход на экран игры
                elif self.button2.is_it(self.mouse_x, self.mouse_y):  # или по другой кнопке был клик
                    return 2  # переход на экран таблицы рекордов
        return self.state  # возврашаем по умолчанию
