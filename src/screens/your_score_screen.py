from src.screens.screen import Screen
from src.buttons.button import Button
import pygame as pg
import os


class YorScoreScreen(Screen):
    button1 = None  # кнопка

    def __init__(self):
        super().__init__("YorScoreScreen", 3, "map.jpg", self.draw)  # передаем основному screen имя , state по -
        # умолчанию имя картинки и функцию рисования деталий

        self.button1 = Button("Top", 192.5, 500, 100, 12)  # создание кнопки

        font = pg.font.Font(None, 80, bold=False, italic=True)  # вызов шрифта для написания текста
        self.text = font.render("The End", True, [10, 10, 20])  # создание текста

        f = open(os.path.join('resources', 'score.txt'), 'r')  # открытие файла
        score = int(f.readline())  # сситование значения
        self.score = font.render("Yor Score:" + str(score), True, [10, 10, 20])  # создание текста с счетом из файла
        f.close()  # закрыть файл

        tmp = []
        f = open(os.path.join('resources', 'thebest.txt'), 'r')  # открытие файла
        line = f.readline()  # сситование значения
        while line:  # сситование значения
            tmp.append(int(line)),  # записся в массив значения
            line = f.readline()  # сситование значения
        f.close()  # закрыть файл

        tmp.append(score)  # добовления в масив результатов твоего резудьтата
        tmp.sort()  # сортировка

        f = open(os.path.join('resources', 'thebest.txt'), 'w')  # записся в файл 10 значений
        for i in range(9, 0, -1):  # записся в файл 10 значений
            f.writelines(str(tmp.pop(i)))  # удаление из маасивав и запись в файл
            f.write("\n")
        f.close()  # закрыть файл

    def draw(self, screen):
        screen.blit(self.text, (190, 60))  # рисование текста
        screen.blit(self.score, (150, 300))  # рисов счета

        self.button1.is_it(self.mouse_x, self.mouse_y)  # проверка наведена ли мышь на кнопку
        self.button1.draw(screen)  # рисование кнопки

    def events_processing(self, events):
        for event in events:
            if self.off_check(event):  # проверка не нажат ли крестик
                return -1
            elif event.type == pg.MOUSEBUTTONDOWN:  # проверка был ли клик мышки если был то
                if self.button1.is_it(self.mouse_x, self.mouse_y):  # по кнопке ли был клик
                    return 2  # переход на экран таблицы рекордов
        return self.state
