from src.func.colors import black
from src.screens.screen import Screen
from src.buttons.button import Button
import pygame as pg
import os


class ScoreScreen(Screen):
    button1 = None
    top = []

    def __init__(self):
        super().__init__("ScoreScreen", 2, "map.jpg", self.draw)  # передаем основному screen имя , state по умолчанию -
        # имя картинки и функцию рисования деталий

        self.button1 = Button("Back", 192.5, 500, 100, 12)  # создание кнопок с надписяю в координатах

        font = pg.font.Font(None, 80, bold=False, italic=True)
        self.text = font.render("The Bests", True, [200, 10, 160])

        self.top = []
        font = pg.font.Font(None, 45, bold=False, italic=True)  # вызов шрифта для написания текста
        f = open(os.path.join('resources', 'thebest.txt'), 'r')  # открыть файл
        line = f.readline()  # читать файл
        while line:  # читать файл пока можешь
            self.top.append(font.render("You : " + line, True, black)),  # создание строчки текста
            line = f.readline()  # читать дальше
        f.close()  # закрыть файл

    def draw(self, screen):
        screen.blit(self.text, (170, 60))  # рисуем текст
        i = 0
        for t in self.top:
            screen.blit(t, (260, i * 40 + 130))  # рисуем все очки
            i += 1

        self.button1.is_it(self.mouse_x, self.mouse_y)  # проверка находится ли мышь над кнопкой
        self.button1.draw(screen)  # рисование кнопки

    def events_processing(self, events):
        for event in events:
            if self.off_check(event):  # проверка не нажат ли крестик
                return -1
            elif event.type == pg.MOUSEBUTTONDOWN:  # проверка был ли клик мышки если был то
                if self.button1.is_it(self.mouse_x, self.mouse_y):  # по кнопке ли был клик
                    return 0  # переход на главный экран
        return self.state
