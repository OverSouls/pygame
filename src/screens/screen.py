import pygame as pg
from src.func.colors import *  # загружаем цвета
from src.func.loadImage import load_image  # import функции load_image


class Screen:  # основной экран из которого создаются остальные экраны
    name = ""  # имя экрана
    state = -2  # state для перехода на экран по умолчанию
    mouse_x, mouse_y = 0, 0  # положение мыши x, y
    back = None  # картинка фона
    draw = None  # функция рисуящая детали

    def __init__(self, name, state, back, draw):
        pg.mouse.set_visible(False)  # отключаем видимость мыши
        self.name = name  # даем имя
        self.state = state  # устанавливае на какой экран переход по умолчанию
        self.back, b = load_image("backs", back)  # загружаем фон спомощью функции loadimage
        self.draw = draw  # функция которя будет рисовать детали

    def main_draw(self, screen):
        # self.mouse_x, self.mouse_y = pg.mouse.get_pos()

        screen.blit(self.back, (0, 0))  # рисуем фон (картинку)

        self.draw(screen)  # рисуем детали

        self.mouse(screen)  # рисуем свою мышь

    def mouse(self, screen):  # рисуем свою мышь
        pg.draw.circle(screen, black, [5 + self.mouse_x, 5 + self.mouse_y], 9, 0)  # рисуем свою мышь

    def events_processing(self, events):  # обработка событий
        return self.state

    def off_check(self, event):
        if event.type == pg.MOUSEMOTION:  # получаем координаты указаателя мыши
            self.mouse_x, self.mouse_y = event.pos
        elif event.type == pg.QUIT:  # проверяем нажата ли кномка выключить (крестик)
            # если собэтие это событие выход (нажатие на крестик)
            self.state = -1  # переключени state на -1 , close в app.py
            return True
