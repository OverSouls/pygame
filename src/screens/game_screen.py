import random
import pygame as pg
import time
import os
from src.screens.screen import Screen
from src.func.loadImage import load_image
from src.buttons.button import Button
from src.func.get_name import get_names
from src.func.get_name import get_name
from src.func.colors import black


class GameScreen(Screen):
    names = get_names()  # получение всех имен в случайном порядке
    level = None  # уровень создастся потом
    score = 0  # счет
    i = 193  # количество оставшихся уровней
    live = False  # активен ли уровени (жив ли)
    timer = 0  # время коца уровня

    def __init__(self):
        super().__init__("GameScreen", 1, "back.jpg", self.draw)  # передаем основному screen имя , state по умолчанию -
        # имя картинки и функцию рисования деталий

        self.font = pg.font.Font(None, 45, bold=False, italic=True)  # вызов шрифта для написания текста
        self.new_level()  # создание 1 уровня

    def new_level(self):  # создание уровня
        self.level = Level(self.get_name())  # создание уровня по доступному именя
        self.live = True  # уровень идет (жив)
        self.i -= 1  # количество оставщихся уровней -1

    def get_name(self):
        return self.names.pop(random.randint(0, self.i))  # возвращает доступное имя и удаляет его

    def draw(self, screen):
        screen.blit(self.font.render("Score: " + str(self.score), True, [30, 60, 80]), (400, 5))  # нарисовать счет

        self.level.draw(screen, self.mouse_x, self.mouse_y)  # нарисовать уровень

    def events_processing(self, events):  # обработка событий
        if self.live:  # еслиуровень идеть
            for event in events:
                if self.off_check(event):  # если не выкл
                    return -1
                elif event.type == pg.MOUSEBUTTONDOWN:  # если кнопка нажата
                    if self.level.events_processing(self.mouse_x, self.mouse_y):  # дать уровню обработать события
                        # и если уровень закончен обновить счет
                        self.score += self.level.lives
                        self.live = False  # уровен кончен
                        self.timer = time.time()  # время конца уровня
        elif time.time() - self.timer > 4:  # прошло ли 4 сек с конца уровня
            if self.i == -1:  # если нет больше уровней записать счет
                f = open(os.path.join('resources', 'score.txt'), 'w')
                f.write(str(self.score) + "\n")
                # f.write(str(time.time()))
                f.close()
                return 3  # открыть экран отображения счета
            else:
                self.new_level()  # иначе создать новый уровень
        return self.state


class Level:
    buttons = []  # масив кнопок
    image = None  # флаг
    heart = None  # жизнь картинка
    lives = 4  # жизни колл

    def __init__(self, ans):
        self.buttons = []
        image, b = load_image("flags-normal", ans + ".png")  # загрузка флага по имени
        self.image = pg.transform.scale(image, (550, 350))  # маштабирование
        self.heart = load_image("lives", "heart" + ".png")[0]  # загрузка сердца
        self.heart.set_colorkey(black)  # очистка фона
        self.heart = pg.transform.scale(self.heart, (40, 40))  # маштабирование
        names = []
        rand = random.randint(0, 5)  # создание масива из 6 имен стран с одним правельным
        for i in range(0, 6, 1):
            if i == rand:
                names.append(ans[0].upper() + ans[1:])  # создание масива из 6 имен стран с одним правельным
            else:
                a = get_name()  # сслучайное имя
                names.append(a[0].upper() + a[1:])  # создание масива из 6 имен стран с одним правельным

        for j in range(0, 2, 1):  # создание масива кнопок с именами из names и пометка правильной
            for i in range(0, 3, 1):
                self.buttons.append(Button(names[i + j * 3], 45 + j * 320, 460 + i * 80, 10, 10))
                if i + j * 3 == rand:
                    self.buttons[rand].flag = True

    def draw(self, screen, x, y):
        for button in self.buttons:  # нарисовать массив кнопок
            button.is_it(x, y)
            button.draw(screen)

        for i in range(0, self.lives, 1):  # нарисовать массив сердец
            screen.blit(self.heart, (10 + i * 60, 10))
        screen.blit(self.image, (48, 59))

    def events_processing(self, x, y):
        for button in self.buttons:
            if button.is_it(x, y):  # проверка нажата ли кнопка
                if button.pressed():  # если кнопка верная то звук верного ответа
                    sound = pg.mixer.Sound(os.path.join('resources', 'sounds', 'zvuk-pravil-nyy-otvet.ogg'))
                    sound.play()
                    return True  # закончить уровен
                else:  # если кнопка не верная то звук верного ответа
                    sound = pg.mixer.Sound(os.path.join('resources', 'sounds', 'zvuk-ne-pravil-nyy-otvet.ogg'))
                    sound.play()
                    self.lives -= 1  # --жизнь
        if self.lives == 0:  # если кончились жизни нажать все кнопки и закончить уровен
            for button in self.buttons:
                button.pressed()
            return True  # закончить уровен
        return False  # не закончить уровен
