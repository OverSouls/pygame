import random

# имена всех стран
names = [{'au': 'australia'}, {'at': 'austria'}, {'az': 'azerbaijan'}, {'al': 'albania'}, {'dz': 'algeria'},
         {'ao': 'angola'}, {'ad': 'andorra'}, {'ag': 'antigua and barbuda'}, {'ar': 'argentina'}, {'am': 'armenia'},
         {'af': 'afghanistan'}, {'bs': 'bahamas'}, {'bd': 'bangladesh'}, {'bb': 'barbados'}, {'bh': 'bahrain'},
         {'by': 'belarus'}, {'bz': 'belize'}, {'be': 'belgium'}, {'bj': 'benin'}, {'bg': 'bulgaria'},
         {'bo': 'bolivia, plurinational state of'}, {'ba': 'bosnia and herzegovina'}, {'bw': 'botswana'},
         {'br': 'brazil'}, {'bn': 'brunei darussalam'}, {'bf': 'burkina faso'}, {'bi': 'burundi'}, {'bt': 'bhutan'},
         {'vu': 'vanuatu'}, {'hu': 'hungary'}, {'ve': 'venezuela'}, {'vn': 'vietnam'}, {'ga': 'gabon'},
         {'ht': 'haiti'}, {'gy': 'guyana'}, {'gm': 'gambia'}, {'gh': 'ghana'}, {'gt': 'guatemala'},
         {'gn': 'guinea'}, {'gw': 'guinea-bissau'}, {'de': 'germany'}, {'hn': 'honduras'}, {'gd': 'grenada'},
         {'gr': 'greece'}, {'ge': 'georgia'}, {'dk': 'denmark'}, {'dj': 'djibouti'}, {'dm': 'dominica'},
         {'do': 'dominican republic'}, {'eg': 'egypt'}, {'zm': 'zambia'}, {'eh': 'western sahara'},
         {'zw': 'zimbabwe'}, {'il': 'israel'}, {'in': 'india'}, {'id': 'indonesia'}, {'jo': 'jordan'},
         {'iq': 'iraq'}, {'ir': 'iran, islamic republic of'}, {'ie': 'ireland'}, {'is': 'iceland'}, {'es': 'spain'},
         {'it': 'italy'}, {'ye': 'yemen'}, {'cv': 'cape verde'}, {'kz': 'kazakhstan'}, {'kh': 'cambodia'},
         {'cm': 'cameroon'}, {'ca': 'canada'}, {'qa': 'qatar'}, {'ke': 'kenya'}, {'cy': 'cyprus'},
         {'kg': 'kyrgyzstan'}, {'ki': 'kiribati'}, {'cn': 'china'}, {'co': 'colombia'}, {'km': 'comoros'},
         {'cg': 'congo'}, {'cd': 'congo, democratic republic of the'},
         {'kp': "korea, democratic people's republic of"}, {'kr': 'korea, republic of'}, {'cr': 'costa rica'},
         {'ci': "cote d'ivoire"}, {'cu': 'cuba'}, {'kw': 'kuwait'}, {'la': "lao people's democratic republic"},
         {'lv': 'latvia'}, {'ls': 'lesotho'}, {'lb': 'lebanon'}, {'ly': 'libyan arab jamahiriya'},
         {'lr': 'liberia'}, {'li': 'liechtenstein'}, {'lt': 'lithuania'}, {'lu': 'luxembourg'}, {'mu': 'mauritius'},
         {'mr': 'mauritania'}, {'mg': 'madagascar'}, {'mw': 'malawi'}, {'my': 'malaysia'}, {'ml': 'mali'},
         {'mv': 'maldives'}, {'mt': 'malta'}, {'ma': 'morocco'}, {'mh': 'marshall islands'}, {'mx': 'mexico'},
         {'fm': 'micronesia, federated states of'}, {'mz': 'mozambique'}, {'md': 'moldova'}, {'mc': 'monaco'},
         {'mn': 'mongolia'}, {'mm': 'myanmar'}, {'na': 'namibia'}, {'nr': 'nauru'}, {'np': 'nepal'},
         {'ne': 'niger'}, {'ng': 'nigeria'}, {'nl': 'netherlands'}, {'ni': 'nicaragua'}, {'nz': 'new zealand'},
         {'no': 'norway'}, {'ae': 'united arab emirates'}, {'om': 'oman'}, {'pk': 'pakistan'}, {'pw': 'palau'},
         {'pa': 'panama'}, {'va': 'holy see (vatican city state)'}, {'pg': 'papua new guinea'}, {'py': 'paraguay'},
         {'pe': 'peru'}, {'pl': 'poland'}, {'pt': 'portugal'}, {'mk': 'macedonia, the former yugoslav republic of'},
         {'ru': 'russian federation'}, {'rw': 'rwanda'}, {'ro': 'romania'}, {'ws': 'samoa'}, {'sm': 'san marino'},
         {'st': 'sao tome and principe'}, {'sa': 'saudi arabia'}, {'sz': 'swaziland'}, {'sn': 'senegal'},
         {'vc': 'saint vincent and the grenadines'}, {'lc': 'saint lucia'}, {'kn': 'saint kitts and nevis'},
         {'rs': 'serbia'}, {'sc': 'seychelles'}, {'sg': 'singapore'}, {'sy': 'syrian arab republic'},
         {'sk': 'slovakia'}, {'si': 'slovenia'}, {'gb': 'united kingdom'}, {'us': 'united states'},
         {'sb': 'solomon islands'}, {'so': 'somalia'}, {'sd': 'sudan'}, {'sr': 'suriname'}, {'sl': 'sierra leone'},
         {'tj': 'tajikistan'}, {'th': 'thailand'}, {'tw': 'taiwan, province of china'},
         {'tz': 'tanzania, united republic of'}, {'tl': 'timor-leste'}, {'tg': 'togo'}, {'to': 'tonga'},
         {'tt': 'trinidad and tobago'}, {'tv': 'tuvalu'}, {'tn': 'tunisia'}, {'tm': 'turkmenistan'},
         {'tr': 'turkey'}, {'ug': 'uganda'}, {'uz': 'uzbekistan'}, {'ua': 'ukraine'}, {'uy': 'uruguay'},
         {'fj': 'fiji'}, {'ph': 'philippines'}, {'fi': 'finland'}, {'fr': 'france'}, {'hr': 'croatia'},
         {'cf': 'central african republic'}, {'td': 'chad'}, {'me': 'montenegro'}, {'cz': 'czech republic'},
         {'cl': 'chile'}, {'ch': 'switzerland'}, {'se': 'sweden'}, {'lk': 'sri lanka'}, {'ec': 'ecuador'},
         {'gq': 'equatorial guinea'}, {'sv': 'el salvador'}, {'er': 'eritrea'}, {'ee': 'estonia'},
         {'et': 'ethiopia'}, {'za': 'south africa'}, {'jm': 'jamaica'}, {'jp': 'japan'}]


def get_name():  # возврашает случайное имя
    tmp = names.copy()  # копируем список имен
    name = tmp.__getitem__(random.randint(0, 194))  # получаем случайное имя + код
    string = ""
    for key, value in name.items():  # берем только имя
        string = value
    return string  # возврашаем случайное имя


def get_names():  # возврашаем список случайных имен
    ans = []  # cоздаем список
    tmp = names.copy()   # копируем список имен
    for i in range(194, -1, -1):
        name = tmp.pop(random.randint(0, i))    # получаем случайное имя + код
        for key, value in name.items():  # берем только имя
            ans.append(value)  # завписываем имя в список
    return ans  # возврашаем список случайных имен
