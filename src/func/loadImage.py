import os
import pygame as pg


def load_image(folder, name):  # имя папки где лежит изоброжение и имя изображения
    fullname = os.path.join('resources', 'images', folder, name)  # получаем путь до image через имя папки и имя файла
    try:
        image = pg.image.load(fullname)  # загружаем картинку
        image = image.convert()  # ковертируем картинку для отоброжения
        return image, image.get_rect()  # возврашаем картинку
    except pg.error:  # если возникнет ошибка ловим ее
        print("Cannot load image:", name)  # пишем ошибку
    return None, None
