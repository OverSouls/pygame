import pygame as pg  # подключение pygame для краткости используется pg

# подключение  экранов из папоки screen
from src.screens.main_screen import MainScreen  # эранн главного меню
from src.screens.game_screen import GameScreen  # экран игры
from src.screens.score_screen import ScoreScreen  # экран таблицы рекордов
from src.screens.your_score_screen import YorScoreScreen  # экран количества очков после окончания игры


class Game:
    state = -2
    live = True  # флаг если true то приложение работоет. после вызова функции сlose становится false
    name = "Jean"  # имя окна
    size = [640, 720]  # размеры окна
    screen = None  # отображающейся экран

    def change_state(self, state):
        if state != self.state:  # проверяем нужно ли менять экран, если да то переключаем
            self.state = state
            if self.state == 0:
                self.screen = MainScreen()  # переключаем на эранн главного меню
            elif self.state == 1:
                self.screen = GameScreen()  # переключаем на экран игры
            elif self.state == 2:
                self.screen = ScoreScreen()  # переключаем на экран таблицы рекордов
            elif self.state == 3:
                self.screen = YorScoreScreen()  # переключаем на экран количества очков после окончания игры
            elif self.state == -1:
                self.close()  # завершаем если нечего отображать

    def start(self):  # основная функция
        pg.init()  # инициализация библиотеки pygame (включение pygame)
        self.live = True  # флаг если true то приложение работоет. после вызова функции сlose становится false

        pg.display.set_mode(self.size)  # создает окно размером size
        pg.display.set_caption(self.name)

        clock = pg.time.Clock()  # создаем clock для контроля частоты кадров

        self.change_state(0)  # выбераем основной экран

        while self.live:  # пока програма live == True (Жива)  цикл будет работать
            self.screen.main_draw(pg.display.get_surface())  # получаем поверхность для рисования и передаем ее -
            # функции которая будет рисовать

            pg.display.flip()  # отображаем нарисованное

            clock.tick(60)  # устанавливаем частоту кадров 60 fps
            self.change_state(self.screen.events_processing(pg.event.get()))  # получаем и обрабатываем(проверяем) -
            # события. от обработчика получаем какой эран должен быть и проверяем это функцией change_state
            pg.time.delay(10)

    def close(self):
        pg.time.Clock().tick(60)  # устанавливаем частоту кадров 60 fps
        self.live = False  # останавливаем цикл сделав live = False
        pg.quit()  # отключаем pygame


# запуск программы
Game().start()  # запуск основной функции
